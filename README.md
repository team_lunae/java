lunae/java (https://registry.hub.docker.com/u/lunae/java/)
===========================================================

A repository for automated build of a Fedora 21 based container with OpenJDK 1.7 + Maven.
